//
//  AppDelegate.swift
//  LabDrawings
//
//  Created by GW Rodriguez on 1/30/16.
//  Copyright (c) 2016 GW Rodriguez. All rights reserved.
//  Version: 0.4.0

import Cocoa

let dbAppDelegate = no





@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {
    // ************* ivars *******************
    // QLK
    var browser: QLKBrowser = QLKBrowser()
    
    // Menu
    @IBOutlet var menu: NSMenu?
    @IBOutlet var mConnection: NSMenu?
    var mCueLists = NSMenu()
    
    // Preferences
    var resolution: Double = 0.025
    var gridSpacing: CGFloat = 40 {
        didSet {
            let num = NSNumber(float: Float(gridSpacing))
            NSNotificationCenter.defaultCenter().postNotificationName(NotifGridSpacing, object: self, userInfo: ["val" : num])
        }
    }
    
    
    
    // basic functions
    func applicationDidFinishLaunching(aNotification: NSNotification) {
        // setup browser
        browser.delegate = self
        browser.start()
        browser.enableAutoRefreshWithInterval(3)
        
        ldprint("App finished launching", line: __LINE__, toggle: dbAppDelegate)
    }

    
    func applicationWillTerminate(aNotification: NSNotification) {
        // Insert code here to tear down your application
    }
}




// MARK: QLKBrowser Delegate
extension AppDelegate: QLKBrowserDelegate  {
    //updated servers
    func browserDidUpdateServers(browser: QLKBrowser!) {updateServers()}
    func updateServers() {
        ldprint("Servers Updated", line: __LINE__, toggle: dbAppDelegate)
        if browser.servers.count == 0 {
            mConnection?.removeAllItems()
            mConnection?.addItem(NSMenuItem(title: "No Servers", action: nil, keyEquivalent: ""))
        }
    }
    
    
    // updated workspaces
    func serverDidUpdateWorkspaces(server: QLKServer!) {updateWorkspaces()}
    func updateWorkspaces() {
        if let servers = browser.servers as NSArray as? [QLKServer] {
            var i=0
            mConnection?.removeAllItems()
            
            // get server
            for server in servers {
                ldprint("Server: \(server.name) [at] \(server.host)", line: __LINE__, toggle: dbAppDelegate)
                
                // create new menu
                let newServer = NSMenuItem()
                let subMenu = NSMenu()
                newServer.title = server.name
                newServer.enabled = yes
                newServer.submenu = subMenu
                mConnection?.addItem(newServer)
                
                // get workspaces for server
                if let workspaces = server.workspaces as NSArray as? [QLKWorkspace] {
                    for workspace in workspaces {
                        ldprint("\t\(workspace) is connected: \(workspace.connected)", line: __LINE__, toggle: dbAppDelegate)
                        
                        let newWorkspace = NSMenuItem(title: workspace.name, action: Selector("makeConnection:"), keyEquivalent: "")
                        newWorkspace.enabled = yes
                        newWorkspace.tag = i++ // used by Document to setState of items
                        subMenu.addItem(newWorkspace)
                    }
                }
            }
            
            // add cue lists submenu
            mConnection?.addItem(NSMenuItem.separatorItem())
            let mList = NSMenuItem()
            mList.title = "Cue List"
            mList.submenu = mCueLists
            mConnection?.addItem(mList)
            
            // connect if only 1 server and workspace
            if servers.count == 1 && servers[0].workspaces.count == 1 {
                if let document = NSDocumentController.sharedDocumentController().currentDocument as? Document {
                    let wksp = servers[0].workspaces[0] as! QLKWorkspace
                    if wksp.uniqueId != document.workspace?.uniqueId {
                        document.workspaceIndex = 0
                        document.workspace = wksp
                        mConnection?.itemAtIndex(0)?.submenu?.itemAtIndex(0)?.state = NSOnState
                    }
                }
            }
        }
    }
}




// MARK: Notifications

extension AppDelegate {
    
}


















