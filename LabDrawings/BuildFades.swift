import Cocoa


let dbBuildFade: Bool = no


enum BuildVar: String {
    case workspace = "workspace"
    case mapVC = "mapVC"
    case cue = "cue"
    case loop = "loop"
    case pathTime = "pathTime"
}

enum CueEditTypes {
    case name, duration, prewait, postwait, cont
    case target, level, groupMode
    case newFade, newGroup, newWait, newStart
    
    static let fadeData = [name, duration, prewait, postwait, cont]
}

enum BuildIndex {
    case starting
    case fadeBuild
    case fadeSetDuration, fadeSetPrewait, fadeSetPostwait, fadeSetCont, fadeSetTarget, fadeSetLevel
    case fadeGetData, fadeGetTarget, fadeGetLevels
    case waitBuild, waitSetDuration, waitSetCont
    case waitGetData
    case startSetPrewait
    case groupPrep, groupBuild, groupSelect
    case mainGroupBuild, mainGroupSetName, mainGroupMode
    case startSetTarget, startGetData
    case cueSetLevels
    case doNothing
    case error
}

enum NotifType {
    case playbackPosition, cueUpdated, none
}









class BuildFades: NSObject {
    private var workspace: QLKWorkspace?
    private var cue: QLKCue?
    private var loop: Bool = no
    private var pathTime: Double = 0
    private var mapVC: MapViewController?
    
    private var analysis = PathAnalysis()
    private var resolution: Double = 0
    private var buildIndex = BuildIndex.error
    private var iCue: Int = -1
    private var numCues: Int = 0
    private var groupings = [String]()
    private var qlabReady: Bool = no // do I need this?
    
    private var fadeInfo = [CueInfo]()
    private var groupInfo = [CueInfo]()
    private var loopInfo = [CueInfo]()
    private var mainGroupInfo = CueInfo()
    
    
    init?(data: [BuildVar: AnyObject]) {
        super.init()
        mainGroupInfo.type = .mainGroup
        
        for (type, object) in data {
            switch type {
            case .workspace:    workspace = object as? QLKWorkspace
            case .cue:          cue = object as? QLKCue
            case .loop:         if let temp = object as? NSNumber {loop = temp.boolValue}
            case .pathTime:     if let temp = object as? NSNumber {pathTime = temp.doubleValue}
            case .mapVC:        if let temp = object as? MapViewController {mapVC = temp}
            }
        }
        
        // getting global resolution
        resolution = app.resolution
        
        if workspace == nil || cue == nil || mapVC == nil || pathTime == 0 || resolution == 0 {return nil}
    }
    
    
    // basic operating functions
    func start() {
        if !doAnalysis() {return}
        getNumFades()
        buildIndex = .starting
        selectCue(cue!.uid)
    }
}






/*      Proceedure

- analize rawSpkVals
- set numCues

*/


// MARK: - Phase 1
private extension BuildFades {
    func doAnalysis() -> Bool {
        let rawSpkVals = mapVC?.getSpeakerVals()
        if rawSpkVals!.isEmpty {print("getSpeakerVals failed"); return no}
        if analysis.run(rawSpkVals!) {return no}
        return yes
    }
    
    
    func getNumFades() {
        for spkInfo in analysis.data {numCues += spkInfo.inflCount}
    }
}










// MARK: - Phase 2


/*      Proceedure

- build fade
- create info
- loop -> build fade
- set name
- set duration
- set prewait
- set postwait
- set continue
- check data
- set target
- check target
- set level
- check level
- loop -> set name

*/

private extension BuildFades {
    func buildFade() {
        numCues--
        buildIndex = .fadeBuild
        sendMsg(.newFade, info: nil)
    }
    
    
    func buildFadeInfo(uid: String) {
        newInfo(.fade, uid: uid)
        if numCues > 0  {buildFade()}
        else            {configFadeInfo()}
    }
    
    
    func configFadeInfo() {
        var index: Int = 0
        
        for speakerData in analysis.data {
            ldprint("|\tSpeaker \(speakerData.output) inflection count: \(speakerData.inflCount)", line: __LINE__, toggle: dbBuildFade)
            
            index += speakerData.inflCount
            for (i, info) in fadeInfo.enumerate() {
                if i == speakerData.inflCount-1 {info.inflType = i==0 ? .only : .last}
                info.setupFade(speakerData.inflInfo[i], inflectionNum: i, spkOut: speakerData.output)
            }
        }
        setFadeName()
    }
    
    
    func setFadeName() {
        buildIndex = .fadeSetDuration
        sendMsg(.name, info: fadeInfo[iCue])
    }
    
    
    func setFadeDuration() {
        buildIndex = .fadeSetPrewait
        if (fadeInfo[iCue].inflNum != 0) {sendMsg(.duration, info: fadeInfo[iCue])}
        else {setFadePrewait()}
    }
    
    
    func setFadePrewait() {
        buildIndex = .fadeSetPostwait
        if (fadeInfo[iCue].inflNum > 0) {sendMsg(.prewait, info: fadeInfo[iCue])}
        else {setFadePostwait()}
    }
    
    
    func setFadePostwait() {
        buildIndex = .fadeSetCont
        if (fadeInfo[iCue].inflNum > 0) {sendMsg(.postwait, info: fadeInfo[iCue])}
        else {setFadeContinue()}
    }
    
    
    func setFadeContinue() {
        buildIndex = .fadeGetData
        if (fadeInfo[iCue].cont != .QLKCueContinueModeNoContinue) {sendMsg(.cont, info: fadeInfo[iCue])}
        else {setFadeLevel()}
    }
    
    
    func getFadeData() {
        let array = [QLKOSCListNameKey, QLKOSCPreWaitKey, QLKOSCDurationKey, QLKOSCPostWaitKey, QLKOSCContinueModeKey]
        if let data = getDataFrom(fadeInfo[iCue].uid!, keys: array) {
            checkFadeData(data)
        }
        else {error("Couldn't get data for fade: \(fadeInfo[iCue])")}
    }
    
    
    func checkFadeData(data: AnyObject) {
        var resend = [CueEditTypes]()
        for type in CueEditTypes.fadeData {
            if !checkData(data, info: fadeInfo[iCue], type: type) {resend.append(type)}
        }
        
        // check what needs resend
        if !resend.isEmpty {
            resendDataFor(resend)
            return
        }
        
        // all data checks out
        getFadeLevels()
    }
    
    
    func resendDataFor(resend: [CueEditTypes]) { // needs an argument for data
        buildIndex = .doNothing
        for type in resend {sendMsg(type, info: fadeInfo[iCue])}
        getFadeData()
    }
    
    
    func setFadeTarget() {
        buildIndex = .fadeGetTarget
        sendMsg(.target, info: fadeInfo[iCue])
    }
    
    
    func getFadeTarget() {
        if let data = getDataFrom(fadeInfo[iCue].uid!, keys: ["target"]) {
            if !checkData(data, info: fadeInfo[iCue], type: .target) {
                setFadeTarget()
                return
            }
        }
        setFadeLevel()
    }
    
    
    func setFadeLevel() {
        buildIndex = .fadeGetLevels
        sendMsg(.level, info: fadeInfo[iCue])
    }
    
    
    func getFadeLevels() {
        if let data = getDataFrom(fadeInfo[iCue].uid!, keys: ["level"]) {
            if !checkData(data, info: fadeInfo[iCue], type: .level) {
                sendMsg(.level, info: fadeInfo[iCue])
            }
            else {
                if iCue < fadeInfo.count {iCue++; setFadeName()} // setup next
                else {prepLooping()} // ready to move on to next phase
            }
        }
    }
}









// MARK: - Phase 3

/*      Proceedure

- select last fade
- build wait
- create info
- set duration
- set continue
- check
- build start
- create info
- set pre wait
*/

private extension BuildFades {
    func prepLooping() {
        if loop {
            buildIndex = .waitBuild
            selectCue(fadeInfo.last!.uid!)
        }
        else {prepCreateGroups()}
    }
    
    
    func buildWait() {
        buildIndex = .waitSetDuration
        sendMsg(.newWait, info: nil)
    }
    
    
    func setWaitDuration(uid: String?) {
        if uid != nil {newInfo(.wait, uid: uid!)}
        buildIndex = .waitSetCont
        sendMsg(.duration, info: loopInfo[0])
    }
    
    
    func setWaitContinue() {
        buildIndex = .waitGetData
        sendMsg(.cont, info: loopInfo[0])
    }
    
    
    func getWaitData() {
        let array = [QLKOSCDurationKey, QLKOSCContinueModeKey]
        if let data = getDataFrom(loopInfo[0].uid!, keys: array) {
            checkWaitData(data)
        }
    }
    
    
    // TODO: check me
    func checkWaitData(data: AnyObject) { // needs data argument
        if !checkData(data, info: groupInfo[0], type: .duration) {setWaitDuration(nil); return}
        if !checkData(data, info: groupInfo[0], type: .cont) {setWaitContinue(); return}
        buildStart()
    }
    
    
    func buildStart() {
        buildIndex = .startSetPrewait
        sendMsg(.newStart, info: nil)
    }
    
    
    func setStartPrewait(uid: String) {
        newInfo(.start, uid: uid)
        buildIndex = .groupPrep
        sendMsg(.prewait, info: loopInfo[1])
    }
}









// MARK: - Phase 4


/*          Proceedure

- find cues to group, store in groupInfo
- *selects cues to group
- creates group
- creates CueInfo for group, replaces select string in groupInfo
- loop (*)
- sets name
- select all groups

*/

private extension BuildFades {
    func prepCreateGroups() {
        var selected: String = ""
        
        // find fades to group
        for info in fadeInfo {
            if info.inflNum == 0 {selected = info.uid!}
            else if info.inflType == .last {groupings.append(selected)}
            else {selected += ",\(info.uid!)"}
        }
        
        // add loop cues
        if loop {groupings.append("\(loopInfo[0].uid!),\(loopInfo[1].uid!)")}
        
        iCue = -1
        selectForGroup(nil)
    }
    
    
    func selectForGroup(uid: String?) {
        if uid != nil {newInfo(.group, uid: uid!)}
        iCue++
        
        if iCue < groupInfo.count {
            buildIndex = .groupBuild
            selectCue(groupings[iCue])
        }
        else {iCue = -1; setGroupName()}
    }
    
    
    func buildGroup() {
        buildIndex = .groupSelect
        sendMsg(.newGroup, info: nil)
    }
    
    
    func setGroupName() {
        iCue++
        if iCue < groupInfo.count {sendMsg(.name, info: groupInfo[iCue]); return} //TODO: groups dunno name
        else {
            var str = ""
            for (i, info) in groupInfo.enumerate() {
                if i==0 {str = info.uid!}
                else {str += ",\(info.uid!)"}
            }
            
            buildIndex = .mainGroupBuild
            selectCue(str)
        }
    }
}










// MARK: - Phase 5


/*      Procedure

- group
- set name
- set mode
if (start)
- set start target
- set start prewait
- check start
- set sound cue's levels to -inf
- check levels
- fin

*/

private extension BuildFades {
    func buildMainGroup(uid: String) {
        newInfo(.group, uid: uid)
        buildIndex = .mainGroupSetName
        sendMsg(.newGroup, info: nil)
    }
    
    
    func setMainGroupName() {
        buildIndex = .mainGroupMode
        sendMsg(.name, info: mainGroupInfo)
    }
    
    
    func setMainGroupMode() {
        if loop {
            buildIndex = .startSetTarget
            sendMsg(.groupMode, info: mainGroupInfo)
        }
        else {setSoundCueLevels()}
    }
    
    
    func setStartTarget() {
        buildIndex = .startSetPrewait
        workspace!.sendMessage(mainGroupInfo.uid!, toAddress: loopInfo[1].target!)
    }
    
    
    func setStartPrewait() {
        buildIndex = .startGetData
        sendMsg(.prewait, info: loopInfo[1])
    }
    
    
    func checkStart() {
        // check prewait
        if let data = getDataFrom(loopInfo[1].uid!, keys: [QLKOSCPreWaitKey]) {
            if !checkData(data, info: loopInfo[1], type: .prewait) {
                setStartPrewait()
                return
            }
        }
        
        // check target
        if let data = getDataFrom(loopInfo[1].uid!, keys: ["target"]) {
            if !checkData(data, info: loopInfo[1], type: .target) {
                workspace!.sendMessage(mainGroupInfo.uid!, toAddress: loopInfo[1].target!)
                return
            }
        }
        
        // everything checks out
        iCue = -1
        buildIndex = .cueSetLevels
        setSoundCueLevels()
    }
    
    
    func setSoundCueLevels() {
        iCue++
        if iCue < analysis.data.count {
            let speaker = analysis.data[iCue]
            workspace!.cue(cue, updateChannel: speaker.output, level: -119.99)
            return
        }
        checkCueLevels()
    }
    
    
    func checkCueLevels() {
        var resend = [Int]()
        if let levels = getDataFrom(cue!.uid, keys: ["level"]) {
            for info in groupInfo {
                if !checkData(levels[info.output], info: info, type: .level) {resend.append(info.output)}
            }
            
            if !resend.isEmpty {
                buildIndex = .doNothing
                for i in resend {
                    workspace!.cue(cue, updateChannel: i, level: -119.99)
                    checkCueLevels()
                    return
                }
            }
            
            // all done
            finish()
        }
    }
    
    
    func finish() {
        
    }
}












// MARK: - Utility
private extension BuildFades {
    func sendMsg(type: CueEditTypes, info: CueInfo?) {
        let nCue = workspace!.cueWithId(info?.uid)
        
        switch type {
        case .name:         workspace!.cue(nCue, updatePropertySend: info!.name, forKey: QLKOSCNameKey)
        case .target:       workspace!.sendMessage(info!.uid, toAddress: info!.target)
        case .duration:     workspace!.cue(nCue, updatePropertySend: NSNumber(double:info!.duration), forKey: QLKOSCDurationKey)
        case .prewait:      workspace!.cue(nCue, updatePropertySend: NSNumber(double:info!.prewait), forKey: QLKOSCPreWaitKey)
        case .postwait:     workspace!.cue(nCue, updatePropertySend: NSNumber(double:info!.postwait), forKey: QLKOSCPostWaitKey)
        case .cont:         workspace!.cue(nCue, updatePropertySend: NSNumber(int: Int32(info!.cont.hashValue)), forKey: QLKOSCContinueModeKey)
        case .level:        workspace!.cue(nCue, updateChannel: info!.output, level: info!.level)
        case .groupMode:    workspace!.sendMessage(NSNumber(int: 3), toAddress: "/cue_id/\(info!.uid)/mode")
        case .newFade:      workspace!.sendMessage("fade", toAddress: "/new")
        case .newGroup:     workspace!.sendMessage("group", toAddress: "/new")
        case .newStart:     workspace!.sendMessage("start", toAddress: "/new")
        case .newWait:      workspace!.sendMessage("wait", toAddress: "/new")
        }
    }
    
    
    func newInfo(type: CueType, uid: String) {
        let info = CueInfo(id: uid)
        info.type = type
        
        switch type {
        case .fade: fadeInfo.append(info)
        case .group: groupInfo.append(info)
        case .wait: info.setupWait(); fallthrough
        case .start: loopInfo.append(info)
        case .mainGroup: mainGroupInfo.uid = uid
        default: error("newInfo: unknown type: \(type)")
        }
    }
    
    
    func selectCue(uid: String) {
        workspace?.sendMessage(nil, toAddress: "/select_id/\(uid)")
    }
    
    
    func error(msg: String) {
        print("Error in BuildFades: \(msg)")
        iCue = -1
        buildIndex = .error
        numCues = 0
    }
    
    
    func getDataFrom(uid: String, keys: [String]) -> AnyObject? {
        var data: AnyObject?
        let nCue = workspace?.cueWithId(uid)
        
        if keys[0] == "target" || keys[0] == "level" {
            for key in keys {
                switch key {
                case "target": workspace!.cue(nCue, valueForKey: "cueTargetNumber", completion: {block in
                        data = block
                    })
                case "level": workspace!.fetchAudioLevelsForCue(nCue, completion: {block in
                        data = block
                    })
                default: break
                }
            }
        } else {
            do {
                if let json = try NSString.init(data: NSJSONSerialization.dataWithJSONObject(keys, options: []), encoding: NSUTF8StringEncoding) {
                    workspace!.sendMessage(json, toAddress: workspace!.addressForCue(nCue, action: "valuesForKeys"), block: { block in
                        data = block
                    })
                }
            } catch let jErr {error("Coudn't make json object for keys. Error: \(jErr)")}
        }
        
        return data
    }
    
    
    func checkData(data: AnyObject, info: CueInfo, type: CueEditTypes) -> Bool {
        switch type {
        case .name:
            if let dict = data as? [String: String] {if dict[QLKOSCListNameKey] != info.name {return no}}
        case .target:
            if let string = data as? String {if string != info.target {return no}}
        case .duration:
            if let dict = data as? [String: Double] {return closeEnough(dict[QLKOSCDurationKey]! - info.duration)}
        case .prewait:
            if let dict = data as? [String: Double] {return closeEnough(dict[QLKOSCPreWaitKey]! - info.duration)}
        case .postwait:
            if let dict = data as? [String: Double] {return closeEnough(dict[QLKOSCPostWaitKey]! - info.duration)}
        case .cont:
            if let dict = data as? [String: Int] {if dict[QLKOSCContinueModeKey] != info.cont.hashValue {return no}}
        case .level:
            if let array = data as? [Double] {return closeEnough(array[info.output]-info.level)}
        default: break
        }
        return yes // all good
    }
    
    
    // a<=x<=b is abs(x-a) <= abs(b-a)
    func closeEnough(num1: Double) -> Bool {return fabs(num1-0.01) <= fabs(0.01-(-0.01))}
}







// MARK: Notifications
extension BuildFades {
    func playbackPosition(uid: String) {
        
    }
    
    func cueUpdate() {
        
    }
}






// MARK: Parser
private extension BuildFades {
    func parser(type: NotifType, uid: String) {
        switch buildIndex {
            case .starting:         buildFade()
            case .fadeBuild:        buildFadeInfo(uid)
            case .fadeSetDuration:  setFadeDuration()
            case .fadeSetPrewait:   setFadePrewait()
            case .fadeSetPostwait:  setFadePostwait()
            case .fadeSetCont:      setFadeContinue()
            case .fadeSetTarget:    setFadeTarget()
            case .fadeSetLevel:     setFadeLevel()
            case .fadeGetData:      getFadeData()
            case .fadeGetTarget:    getFadeTarget()
            case .fadeGetLevels:    getFadeLevels()
            case .waitBuild:        buildWait()
            case .waitSetDuration:  setWaitDuration(uid)
            case .waitSetCont:      setWaitContinue()
            case .waitGetData:      getWaitData()
            case .startSetPrewait:  setStartPrewait(uid)
            case .groupPrep:        prepCreateGroups()
            case .groupBuild:       buildGroup()
            case .groupSelect:      selectForGroup(uid)
            case .mainGroupBuild:   buildMainGroup(uid)
            case .mainGroupSetName: setMainGroupName()
            case .mainGroupMode:    setMainGroupMode()
            case .startSetTarget:   setStartTarget()
            case .startGetData:     checkStart()
            case .cueSetLevels:     setSoundCueLevels()
            case .doNothing:        break
            case .error:            print("buildIndex error")
        }
    }
}


























