import Cocoa



enum CueType {case audio, fade, group, start, wait, mainGroup, noType}
enum InflType {case last, only, normal, error}


class CueInfo: NSObject {
    var uid: String?
    var name: String {
        get {
            switch type {
            case .audio: return ""
            case .fade: return "Spk %\(output) inf \(inflNum)"
            case .group: return ""
            case .mainGroup: return "Start Path"
            default: return ""
            }
        }
    }
    var type = CueType.noType {
        didSet {
            switch type {
            case .wait:
                duration = 0.01
                cont = .QLKCueContinueModeAutoFollow
            default: break
            }
        }
    }
    
    // fade/wait
    var duration: Double = 0
    var cont = QLKCueContinueMode.QLKCueContinueModeNoContinue
    
    // fade/start
    var prewait: Double = 0 {
        didSet{if (type == .wait) {prewait -= 0.01}}
    }
    
    // fade
    var infl: Inflection?
    var inflType = InflType.error
    var inflNum: Int = 0
    var output: Int = 0
    var level: Double = 0
    var postwait: Double = 0
    var target: String? {get{if type == .fade {return "/cue_id_\(uid)/cueTargetId"} else {return nil}}}
    
    
    override init() {
        super.init()
    }
    
    init(id: String) {
        super.init()
        uid = id
    }
}



// Constructors
extension CueInfo {
    func setupFade(inflection: Inflection, inflectionNum: Int, spkOut: Int) {
        type = .fade
        infl = inflection
        inflNum = inflectionNum
        output = spkOut
        
        if inflNum == 0 {postwait = Double(infl!.end) * app.resolution}
        else {duration = Double(infl!.end) * app.resolution}
        
        prewait = Double(infl!.start) * app.resolution
        
        if inflType != .last {
            if inflNum == 0 {cont = .QLKCueContinueModeAutoContinue}
            else {cont = .QLKCueContinueModeAutoFollow}
        }
        
        level = infl!.val
        if level <= -120 {level = -119.99}
    }
    
    
    func setupWait() {
        type = .wait
        duration = 0.01
        cont = .QLKCueContinueModeAutoContinue
    }
    
    
    func setupStart(prewait: Double) {
        type = .start
        self.prewait = prewait - 0.1
    }
}































