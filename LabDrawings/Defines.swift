import Foundation


// basic macros
let yes = true
let no = false

// debug
let LDDEBUG = no


// App macros
let app = NSApplication.sharedApplication().delegate as! AppDelegate


// identifiers
enum MapTool: Int {
    case listen = -1
    case speaker = -2
    case path = -3
}


// Notifications
let NotifGridSpacing = "NotifGridSpacing"


// Bindings
enum ModelBindings: String {
    case Map = "map"
    case Speaker = "speaker"
    case Path = "path"
    case Point = "point"
}


// Structs
struct SpeakerVals {
    var vals = [Double]()
    var output:Int = 0
}


// basic functions
func ldprint(msg: String, line: Int, toggle: Bool) {
    if toggle || LDDEBUG {
        print("\(msg)\t[on line: \(line)]")
    }
}

func clickCheck(p1: NSPoint, p2: NSPoint, d: CGFloat) -> Bool {
    let d2 = d*0.5
    let a = Float(p1.x - (p2.x+d2))
    let b = Float(p1.y - (p2.y+d2))
    return sqrtf(a*a + b*b) <= Float(d2)
}


func interpolateLinear(x: Double, pt0: NSPoint, pt1: NSPoint) -> Double {
    // y0 + ((y1 - y0) * ((x-x0)/(x1-x0)))
    return Double(pt0.y) + (Double(pt1.y-pt0.y) * (x - Double(pt0.x)) / Double(pt1.x-pt0.x))
}


func ampToDB(amp: Double) -> Double {
    return 45 * log10(amp)
}


// extensions 
extension Double {
    func format(f: String) -> String {
        return String(format: "%\(f)f", self)
    }
}






// QLK Enum's
enum QLKCueContinueMode {
    case QLKCueContinueModeNoContinue
    case QLKCueContinueModeAutoContinue
    case QLKCueContinueModeAutoFollow
}























