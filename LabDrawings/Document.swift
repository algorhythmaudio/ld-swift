import Cocoa

let dbDocument = no


// MARK: - Main

class Document: NSDocument {
    
    // Class variables
    var workspaceIndex: Int = -1 // used for Connection Menu
    var tool: MapTool = .listen
    var wc: DocumentWindowController?
    
    var selectedCueList: QLKCue?
    var selectedCue: QLKCue?
    var workspace: QLKWorkspace? {
        willSet {if workspace != nil {self.workspace!.disconnect()}}
        didSet {
            if workspace != nil {
                makeWorkspaceNotifications()
                workspace!.connect()
                ldprint("New workspace connected: \(workspace!.name)", line: __LINE__, toggle: dbDocument)
            }
        }
    }
    
    
    // window
    override func makeWindowControllers() {
        wc = DocumentWindowController()
        addWindowController(wc!)
    }
    
    
    // MARK: Model
    var maps = [Map]()
    var paths = [Path]()
    
    @IBAction func testing(sender: AnyObject?) {
        print(maps, paths)
    }
}









// MARK: - Document

extension Document {
    
    override class func autosavesInPlace() -> Bool {return false}
    
    
    override func dataOfType(typeName: String) throws -> NSData {
        let temp = NSMutableArray(capacity: 2)
        if !maps.isEmpty {temp.insertObject(maps, atIndex: 0)}
        if !paths.isEmpty {temp.insertObject(paths, atIndex: 1)}

        return NSKeyedArchiver.archivedDataWithRootObject(temp)
    }
    
    override func readFromData(data: NSData, ofType typeName: String) throws {
        var outError: NSError! = NSError(domain: "Migrator", code: 0, userInfo: nil)
        outError = NSError(domain: NSOSStatusErrorDomain, code: unimpErr, userInfo: nil)
        
        // load data
        if let array: NSArray = NSKeyedUnarchiver.unarchiveObjectWithData(data) as? NSArray {
            if let mapData = array[0] as? [Map] {maps = mapData}
            else {throw outError}
            if let pathData = array[1] as? [Path] {paths = pathData}
            else {throw outError}
        }
    }
}







// MARK: - Model

extension Document {
    func newMap() -> Map {
        let map = Map()
        maps.append(map)
        return map
    }
    
    
    func newPath() -> Path {
        let path = Path()
        paths.append(path)
        return path
    }
    
}











// MARK: - Xib

extension Document {
    
    // user selected a workspace to connect to
    @IBAction func makeConnection(sender: NSMenuItem) {
        ldprint("Workspace selected from menu", line: __LINE__, toggle: dbDocument)
        let parent = sender.parentItem!
        
        // set workspace
        workspaceIndex = -1
        var temp: QLKWorkspace?
        for server in app.browser.servers {
            if server.name == parent.title {
                for wksp in server.workspaces as NSArray {
                    workspaceIndex++
                    if wksp.name == sender.title {
                        temp = wksp as? QLKWorkspace
                        break
                    }
                }
            } else {continue}
            if temp != nil {break}
        }
        
        // checks that user didn't click same already connected workspace
        if workspace !== temp {workspace = temp}
    }
    
    
    func populateCues() {
        if (workspace == nil) || (!workspace!.connected) || (workspace!.root.cues == nil) {return}
        
        // first get cue lists
        var cueLists = [QLKCue]()
        if let lists = workspace!.root.cues as NSArray as? [QLKCue] {
            for list in lists {
                if list.uid != QLKActiveCueListIdentifier {cueLists.append(list)}
            }
            
            // prep
            let tempCueList = selectedCueList
            selectedCueList = nil
            app.mCueLists.removeAllItems()
            
            // populate cue list menu
            if cueLists.count > 0 {
                for list in cueLists {
                    let item = NSMenuItem(title: list.listName, action: Selector("connectToCueList:"), keyEquivalent: "")
                    item.representedObject = list
                    item.tag = -99 // id for cue lists
                    app.mCueLists.addItem(item)
                    if tempCueList === list {selectedCueList = tempCueList}
                }
            }
            
            if cueLists.count == 1 {
                selectedCueList = cueLists[0]
                addCuesToCombo()
            }
            else if selectedCueList == nil {wc!.cues!.removeAllItems()}
            else {addCuesToCombo()}
        }
    }
    
    
    // adds the QLKCue to the represented object of the menu item
    private func addCuesToCombo() {
        if selectedCueList == nil {return}
        
        // prep
        wc!.cues!.removeAllItems()
        let cuesDisplay = wc!.cues!.menu!
        let disconnect = NSMenuItem(title: "Disconnect", action: Selector("disconnectFromCue:"), keyEquivalent: "")
        cuesDisplay.addItem(disconnect)
        cuesDisplay.addItem(NSMenuItem.separatorItem())
        
        // add cues
        if let listCues = selectedCueList!.cues as NSArray as? [QLKCue] {
            for cue in listCues {
                if cue.type == QLKCueTypeAudio {
                    let name = "\(cue.number) - \(cue.listName)"
                    addCueToPopup(name, cue: cue, cueList: cuesDisplay)
                }
                else if cue.type == QLKCueTypeGroup {
                    let name = "\(cue.number) - (\(cue.listName))"
                    recursiveAddGroupCues(cue, cueList: cuesDisplay, name: name)
                }
            }
        }
    }
    
    
    private func recursiveAddGroupCues(group: QLKCue, cueList: NSMenu, name: String) {
        if let cues = group.cues as NSArray as? [QLKCue] {
            var title = name
            
            for cue in cues {
                if cue.type == QLKCueTypeAudio {
                    title += " \(cue.listName)"
                    addCueToPopup(title, cue: cue, cueList: cueList)
                }
                else if cue.type == QLKCueTypeGroup {
                    title += " (\(cue.listName))"
                    recursiveAddGroupCues(cue, cueList: cueList, name: title)
                }
            }
        }
    }
    
    
    private func addCueToPopup(name: String, cue: QLKCue, cueList: NSMenu) {
        let item = NSMenuItem(title: name, action: Selector("connectToCue:"), keyEquivalent: "")
        item.representedObject = cue
        item.tag = 0
        cueList.addItem(item)
    }

    
    func connectToCueList(sender: NSMenuItem) {
        if let list = sender.representedObject as? QLKCue {
            selectedCueList = list
            selectedCue = nil
            addCuesToCombo()
        }
    }
    
    func connectToCue(sender: NSMenuItem) {
        if let cue = sender.representedObject {
            selectedCue = cue as? QLKCue
            wc!.setPopupSize()
        }
    }
    
    func disconnectFromCue() {
        selectedCue = nil
        wc!.setPopupSize() // length of the string "disconnect"
    }
    
    
    // sets state for menu items
    override func validateMenuItem(menuItem: NSMenuItem) -> Bool {
        if menuItem.tag >= 0 {menuItem.state = workspaceIndex == menuItem.tag ? NSOnState : NSOffState}
        else if menuItem.tag == -99 {menuItem.state = selectedCueList === menuItem.representedObject ? NSOnState : NSOffState}
        else {menuItem.state = tool.rawValue == menuItem.tag ? NSOnState : NSOffState}
        
        return true
    }
    
    
    // user selected tool
    @IBAction func toolSelected(sender: AnyObject) {
        ldprint("User selected a new tool", line: __LINE__, toggle: dbDocument)
        
        switch sender.tag() {
            case -1: tool = .listen
            case -2: tool = .speaker
            case -3: tool = .path
            default: print("tag error for tool selector")
        }
        
        updateSelectedTool()
    }
    
    
    @IBAction func build(sender: AnyObject) {
        wc!.build()
    }
}






// MARK: - Notifications
extension Document {
    func makeWorkspaceNotifications() {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("workspaceDisconnected:"), name: QLKWorkspaceDidDisconnectNotification, object: workspace)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("workspaceCuesUpdated:"), name: QLKWorkspaceDidUpdateCuesNotification, object: workspace)
    }
    
    
    func breakWorkspaceNotifications() {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: QLKWorkspaceDidDisconnectNotification, object: workspace)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: QLKWorkspaceDidUpdateCuesNotification, object: workspace)
    }
    
    
    func workspaceCuesUpdated(notif: NSNotification) {populateCues()}
    func workspaceDisconnected(notif: NSNotification) {breakWorkspaceNotifications()}
}












// MARK: - Utiltiy
private extension Document {
    func updateSelectedTool() {
        wc!.tool = tool
    }
}


























