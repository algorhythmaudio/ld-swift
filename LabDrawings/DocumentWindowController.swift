import Cocoa



class DocumentWindowController: NSWindowController {
    
    var inspectorOut: Bool = no
    @IBOutlet weak var splitView: NSSplitView!
    @IBOutlet weak var splitViewTop: NSScrollView!
    
    var mapVC: MapViewController?
    var cues: NSPopUpButton?
    
    var inspectorVC: InspectorViewController?
    var tool: MapTool = .listen {didSet{updatedTool()}}
    
    
    // Init
    convenience init() {self.init(windowNibName: "DocumentWindowController")}
    
    override func windowDidLoad() {
        super.windowDidLoad()
        
        // setup map view
        mapVC = MapViewController()
        if mapVC != nil {
            mapVC!.wc = self
            
            mapVC!.view.frame = NSMakeRect(0, 0, frameSize.width, frameSize.height)
            mapVC!.view.bounds = NSMakeRect(0, 0, frameSize.width, frameSize.height)
            splitViewTop.documentView = mapVC!.view
            
            // setup combo box
            cues = NSPopUpButton(frame: popupRect(110), pullsDown: no)
            cues!.autoenablesItems = no
            splitViewTop.addSubview(cues!)
        } else {
            // error
        }
        
        // setup inspector
        inspectorVC = InspectorViewController()
        if inspectorVC != nil {splitView.addSubview(inspectorVC!.view)}
        
        // if no map/path create new, if opening a doc load first map/path
        if let doc = getDocument() {
            if doc.maps.isEmpty {newMap()}
            else {setMap(doc.maps[0])}
            if doc.paths.isEmpty {newPath()}
            else {setPath(doc.paths[0])}
        }
        
        window?.center()
        splitView.setPosition(inspectorSize(), ofDividerAtIndex: 0)
    }
    
    
    // MARK: Split View
    
    func splitView(splitView: NSSplitView, canCollapseSubview subview: NSView) -> Bool {return no}
    func splitView(splitView: NSSplitView, constrainMinCoordinate proposedMin: CGFloat, ofSubviewAt dividerIndex: Int) -> CGFloat {
        return proposedMin + inspectorSize()
    }
    func splitView(splitView: NSSplitView, constrainSplitPosition proposedPosition: CGFloat, ofSubviewAt dividerIndex: Int) -> CGFloat {
        return proposedPosition - inspectorSize()
    }
}



// MARK: - Accessors

extension DocumentWindowController {
    // gets the size of the string in pixles and sets the popup width
    func setPopupSize() {
        let name = NSString(string: cues!.selectedItem!.title)
        let size = name.sizeWithAttributes([NSFontAttributeName: cues!.font!])
        cues!.frame = popupRect(size.width+35)
    }
    
    
    func build() {
        mapVC?.build()
    }
}



// MARK: Xib

private extension DocumentWindowController {
    @IBAction func toggleInspector(sender: NSMenuItem) {
        inspectorOut = inspectorOut ? false : true
        splitView.setPosition(inspectorSize(), ofDividerAtIndex: 0)
    }
    
    
    @IBAction func addSpeaker(sender: AnyObject) {
        let view = splitViewTop.visibleRect
        let point = NSMakePoint(NSMidX(view), NSMidY(view))
        mapVC?.newSpeakerAtPoint(point)
    }
}





// MARK: - Model

extension DocumentWindowController {
    func newMap() {if let map = getDocument()?.newMap() {setMap(map)}}
    func newPath() {if let path = getDocument()?.newPath() {setPath(path)}}
    
    func setMap(map: Map) {mapVC!.map = map}
    func setPath(path: Path) {mapVC!.path = path}
}



// MARK: - Utility

private extension DocumentWindowController {
    
    func inspectorSize() -> CGFloat {
        if var windowSize:CGFloat = window?.frame.size.height {
            windowSize -= inspectorOut ? InspectorSize.tall.rawValue : InspectorSize.small.rawValue
            return windowSize
        }
        return 0
    }
    
    
    func popupRect(width: CGFloat) -> NSRect {
        var rect = splitViewTop.visibleRect
        rect.offsetInPlace(dx: rect.size.width-(width+17), dy: 5)
        rect.size = NSMakeSize(width, 26)
        return rect
    }
    
    
    func updatedTool() {
        if inspectorVC?.current != .small {
            switch tool {
            case .listen:   inspectorVC?.current = .listen
            case .speaker:  inspectorVC?.current = .speaker
            case .path:     inspectorVC?.current = .path
            }
        }
    }
    
    
    func getDocument() -> Document? {return document as? Document}
}


















