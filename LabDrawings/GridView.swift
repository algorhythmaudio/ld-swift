import Cocoa

let dbGridView: Bool = no

class GridView: NSObject {
    
    private var gridSpacing:CGFloat = 0
    
    
    override init() {
        super.init()
        gridSpacing = app.gridSpacing
        //NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("updateGridSpacing:"), name: NotifGridSpacing, object: nil)
    }
    //deinit {NSNotificationCenter.defaultCenter().removeObserver(self)}
    
    
    func drawRect(rect: NSRect, forView view: NSView) {
        let grid = NSBezierPath()
        var lastLine = (Int)(floor(NSMaxX(rect) / gridSpacing))
        
        for (var lineNumber = Int(ceil(NSMinX(rect)) / gridSpacing); lineNumber <= lastLine; lineNumber++) {
            grid.moveToPoint(NSMakePoint((CGFloat(lineNumber) * gridSpacing), NSMinY(rect)))
            grid.lineToPoint(NSMakePoint((CGFloat(lineNumber) * gridSpacing), NSMaxY(rect)))
        }
        
        lastLine = Int(floor(NSMaxY(rect) / gridSpacing))
        for (var lineNumber = Int(ceil(NSMinY(rect) / gridSpacing)); lineNumber <= lastLine; lineNumber++) {
            grid.moveToPoint(NSMakePoint(NSMinX(rect), CGFloat(lineNumber) * gridSpacing))
            grid.lineToPoint(NSMakePoint(NSMaxX(rect), CGFloat(lineNumber) * gridSpacing))
        }
        
        LDColor.grid()
        grid.lineWidth = 0.0
        grid.stroke()
    }
}


extension GridView {
//    func updateGridSpacing(notif: NSNotification) {
//        ldprint("\(notif)", __LINE__, dbGridView)
//        
//        if let info:Dictionary = notif.userInfo {
//            let num = info["val"] as! NSNumber
//            gridSpacing = CGFloat(num.floatValue)
//            needsDisplay = true
//        }
//    }
}
