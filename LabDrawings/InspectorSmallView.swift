//
//  InspectorSmallView.swift
//  LabDrawings
//
//  Created by GW Rodriguez on 3/21/16.
//  Copyright © 2016 GW Rodriguez. All rights reserved.
//

import Cocoa

class InspectorSmallView: NSView {
    
    
    override init(frame frameRect: NSRect) {
        super.init(frame: frameRect)
        
    }

    required init?(coder: NSCoder) {super.init(coder: coder)}

    
    override func drawRect(dirtyRect: NSRect) {
        super.drawRect(dirtyRect)

        // Drawing code here.
    }
    
}
