import Cocoa


enum InspectorSize: CGFloat {
    case small = 70
    case tall = 200
}
enum InspectorView {case small, listen, speaker, path}


class InspectorViewController: NSViewController {
    var current: InspectorView = .small {didSet{changedView()}}
    
    var smallView = InspectorSmallView(frame: NSMakeRect(0, 0, frameSize.width, InspectorSize.small.rawValue))
    var listenView = InspectorListenView(frame: NSMakeRect(0, 0, frameSize.width, InspectorSize.tall.rawValue))
    var speakerView = InspectorSpeakerView(frame: NSMakeRect(0, 0, frameSize.width, InspectorSize.tall.rawValue))
    var pathView = InspectorPathView(frame: NSMakeRect(0, 0, frameSize.width, InspectorSize.tall.rawValue))
    

    override func loadView() {view = smallView}

//    override func viewDidLoad() {
//        if #available(OSX 10.10, *) {
//            super.viewDidLoad()
//            
//        } else {
//            
//        }
//    }
    
}






// MARK: - Utility

private extension InspectorViewController {
    func changedView() {
        switch current {
        case .small: view = smallView
        case .listen: view = listenView
        case .speaker: view = speakerView
        case .path: view = pathView
        }
        
        view.needsDisplay = yes
    }
}













