import Cocoa

class LDColor: NSColor {
    
    class func background() -> CGColorRef {return NSColor(deviceRed: 0.19, green: 0.19, blue: 0.19, alpha: 1.0).CGColor} // 333333
    class func grid()       {NSColor.whiteColor().set()}
}


// MARK: - Speaker
extension LDColor {
    class func speaker(selected: Bool) {
        if !selected {
            NSColor(deviceRed: 0.7, green: 0.7, blue: 0.7, alpha: 1.0).set() // 999999
        } else {
            NSColor(deviceRed: 0.8, green: 0.8, blue: 0.8, alpha: 1.0).set() // cccccc
        }
    }
    
    class func outlineSpeaker(selected: Bool) {
        if !selected {
            
        } else {
            
        }
        NSColor(deviceRed: 0.3, green: 0.3, blue: 0.3, alpha: 1.0).set() // i set this
    }
    
    
    class func power(selected: Bool) {
        var red, green, blue: CGFloat
        
        if !selected    {red = 0.039; green = 0.549; blue = 0.702;} // 0a8cb3
        else            {red = 0.698; green = 0.067; blue = 0.255;} // b21141
        
        NSColor(deviceRed: red, green: green, blue: blue, alpha: 0.3).set()
    }
    
    class func outlinePower(selected: Bool) {
        var red, green, blue: CGFloat
        
        if !selected    {red = 0.012; green = 0.157; blue = 0.2;} // 032833
        else            {red = 0.996; green = 0.396; blue = 0.576;} // FE6593
        
        NSColor(deviceRed: red, green: green, blue: blue, alpha: 0.3).set()
    }
    
    
    class func radius(selected: Bool) {
        var red, green, blue: CGFloat
        
        if !selected    {red = 0.039; green = 0.549; blue = 0.702;} // 0a8cb3
        else            {red = 0.698; green = 0.067; blue = 0.255;} // b21141
        
        NSColor(deviceRed: red, green: green, blue: blue, alpha: 0.3).set()
    }
    
    class func outlineRadius(selected: Bool) {
        var red, green, blue: CGFloat
        
        if !selected    {red = 0.012; green = 0.157; blue = 0.2;} // 032833
        else            {red = 0.996; green = 0.396; blue = 0.576;} // FE6593
        
        NSColor(deviceRed: red, green: green, blue: blue, alpha: 0.3).set()
    }

}




// MARK: - Points
extension LDColor {
    class func pathPoint() -> NSColor {return NSColor(deviceRed: 0.969, green: 0.953, blue: 0.243, alpha: 1.0)} // ?
    class func pathLine() {return NSColor(deviceRed: 0.502, green: 0.471, blue: 0.078, alpha: 1.0).set()} // 807814
    
    class func listen() -> NSColor {return NSColor(deviceRed: 0.024, green: 0.702, blue: 0.028, alpha: 1.0)} // 06B307
    
    class func pointBorder() {NSColor.blackColor().set()}
}














