import Cocoa


let dbMap: Bool = no


class Map: NSObject, NSCoding {
    var name: String = "New Map"
    var speakers = [Speaker]()
    var numOfSpeakers: Int {return speakers.count}
    var usedOutputs:[Bool] = []
    
    var selected = -1 {
        didSet {
            if oldValue > -1 {speakers[oldValue].isSelected = no}
            if selected > -1 {speakers[selected].isSelected = yes}
        }
    }
    
    
    override init() {
        super.init()
        prepUsedOutputs()
    }
    
    
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(name, forKey: "name")
        aCoder.encodeObject(NSKeyedArchiver.archivedDataWithRootObject(speakers), forKey: "speakers")
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init()
        
        var err: Int = 0
        
        prepUsedOutputs()
        
        // set name
        if let newName = aDecoder.decodeObjectForKey("name") as? String {
            name = newName
            ldprint("|\tname: \(name)", line: __LINE__, toggle: dbMap)
        } else {err -= 1}
        
        // set speakers
        if let data = aDecoder.decodeObjectForKey("speakers") as? NSData {
            if let newSpeakers = NSKeyedUnarchiver.unarchiveObjectWithData(data) as? [Speaker] {
                speakers = newSpeakers
                ldprint("|\tspeakers: \(speakers)", line: __LINE__, toggle: dbMap)
                for spk in speakers {
                    ldprint("|\t|\tcenter: \(spk.center)", line: __LINE__, toggle: dbMap)
                }
            } else {err -= 4}
        } else {err -= 2}
        
        // set usedOutputs
        for spk in speakers {
            usedOutputs[spk.output-1] = yes
        }
        
        if err != 0 {assertionFailure("Map decode error: \(err)")}
    }
}




// MARKL - Modification
extension Map {
    func addSpeakerAtPoint(point: NSPoint) -> Bool {
        //speakers += Speaker(atPoint: point, output: 1)
        let output = nextAvailableSpeaker()
        if output == 0 {return false}
        
        speakers.append(Speaker(point: point, forOutput: output))
        selected = speakers.count - 1
        
        return true
    }
    
    
    // called by MapView
    func drawSpeakers() {
        // draw radius, power, speaker
        for speaker in speakers {
            if !speaker.isSelected {speaker.drawRadius()}
        }
        
        for speaker in speakers {
            if !speaker.isSelected {speaker.drawPower()}
        }
        
        for speaker in speakers {
            if speaker.isSelected {
                speaker.drawRadius()
                speaker.drawPower()
                break
            }
        }
        
        for speaker in speakers {
            if !speaker.isSelected {speaker.drawSpeaker()}
        }
        
        for speaker in speakers {
            if speaker.isSelected {
                speaker.drawSpeaker()
                return
            }
        }
    }
}





// MARK: - Utility
extension Map {
    func nextAvailableSpeaker() -> Int {
        for (var i=0; i<usedOutputs.count; i++) {
            if usedOutputs[i] == no {return i+1}
        }
        return 0 // no available outputs
    }
    
    
    func getSelectedSpeaker() -> Speaker? {
        if selected == -1 || speakers.isEmpty {return nil}
        return speakers[selected]
    }
    
    
    func prepUsedOutputs() {for _ in 1...48 {usedOutputs.append(no)}}
}



















