import Cocoa


let dbMapView = no
let frameSize: NSSize = NSMakeSize(2000, 2000)


enum Dragging {
    case nothing
    case listen
    case speaker
    case point
}


class MapView: NSView {
    var vc: MapViewController!
    
    private var listen = Point(center: NSMakePoint(10, 10))
    private var _grid = GridView()
    private var drag: Dragging = .nothing
    
    
    override init(frame frameRect: NSRect) {
        super.init(frame: frameRect);
        wantsLayer = yes
        layer?.backgroundColor = LDColor.background()
        
        listen.size = 11
        listen.color = LDColor.listen()
    }
    
    required init?(coder: NSCoder) {super.init(coder: coder)}
    
    
    override func drawRect(dirtyRect: NSRect) {
        NSGraphicsContext.saveGraphicsState()
        
        _grid.drawRect(dirtyRect, forView: self)
        vc.map?.drawSpeakers()
        listen.draw()
        
        NSGraphicsContext.restoreGraphicsState()
    }
}





// MARK: - Mouse

extension MapView {
    // may or maynot need this
    //override func acceptsFirstMouse(theEvent: NSEvent) -> Bool {return yes}
    
    override func mouseDown(theEvent: NSEvent) {
        let mouse = convertPoint(theEvent.locationInWindow, fromView: nil)
        let tool = document()!.tool
        
        vc.map?.selected = -1
        vc.path?.selected = -1
        
        switch tool {
        case .listen:
            drag = .listen
            listen.center = mouse
        case .speaker:
            if vc.map == nil {return}
            // check double click to add
            if theEvent.clickCount > 1 {
                // check not on top of another speaker
                for (var i=0; i<vc.map?.speakers.count; i++) {
                    let spk = vc.map!.speakers[i]
                    if clickCheck(mouse, p2: spk.speaker.origin, d: spk.speaker.height) {return}
                }
                // if we got this far, then add a speaker
                vc.map?.addSpeakerAtPoint(mouse)
            }
            // single click
            else {
                for (var i=0; i<vc.map?.speakers.count; i++) {
                    let spk = vc.map!.speakers[i]
                    if clickCheck(mouse, p2: spk.speaker.origin, d: spk.speaker.height) {
                        vc.map!.selected = i
                        drag = .speaker
                        break
                    }
                }
            }
        case .path:
            if vc.path == nil {return}
            if theEvent.clickCount > 1 {
                let newPoint = Point(center: mouse)
                drag = .point
                
                // check if between two points
                if vc.path?.points.count > 1 {
                    for (var i=0; i<vc.path!.points.count-1; i++) {
                        let pt1 = vc.path!.points[i]
                        let pt2 = vc.path!.points[i+1]
                        
                        if isClickNearLine(mouse, point1: pt1.center, point2: pt2.center) {
                            vc.path!.points.insert(newPoint, atIndex: i+1)
                            vc.path!.selected = i+1
                            needsDisplay = yes
                            return
                        }
                    }
                }
                
                // not between points
                vc.path!.points.append(newPoint)
                vc.path!.selected = vc.path!.points.count - 1
            }
            else {
                // single click
                vc.path!.selected = 0
                for point in vc.path!.points {
                    if clickCheck(mouse, p2: point.drawPoint, d: point.size) {
                        drag = .point
                        needsDisplay = yes
                        return
                    }
                    vc.path!.selected++
                }
                vc.path!.selected = -1
            }
        }
        
        needsDisplay = yes
    }
    
    
    override func mouseDragged(theEvent: NSEvent) {
        if drag == .nothing {return}
        
        let mouse = convertPoint(theEvent.locationInWindow, fromView: nil)
        //let tool = document()!.tool
        
        switch drag {
        case .listen:
            listen.center = mouse
        case .speaker:
            let spk = vc.map?.getSelectedSpeaker()
            spk?.center = mouse
        case .point:
            vc.path?.getSelectedPoint()?.center = mouse
        default: return
        }
        needsDisplay = yes
    }
    
    
    override func mouseUp(theEvent: NSEvent) {
        //let mouse = convertPoint(theEvent.locationInWindow, fromView: nil)
        //let tool = document()!.tool
        
        drag = .nothing
        needsDisplay = yes
    }
}




// MARK: - Model
extension MapView {
    func currentMap() -> Map {
        return Map()
    }
}





// MARK: Notifications
extension MapView {
    
}




// MARK: Utility
extension MapView {
    func document() -> Document? {
        return NSDocumentController.sharedDocumentController().documentForWindow(window!) as? Document
    }
}



















