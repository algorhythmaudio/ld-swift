import Cocoa

let dbMapVC = no





class MapViewController: NSViewController {
    var wc: DocumentWindowController?
    var mapView = MapView(frame: NSMakeRect(0, 0, frameSize.width, frameSize.height))
    
    var map: Map?
    var path: Path?
    
    // used for getting vals for a path
    private var interpVal: Double = 0
    private var interpTick: Double = 0
    
    
    override func loadView() {view = mapView; mapView.vc = self}
}





// MARK: - Model
extension MapViewController {
    func newSpeakerAtPoint(point: NSPoint) {
        map?.addSpeakerAtPoint(point)
        mapView.needsDisplay = yes
    }
}





// MARK: - Getting Values
extension MapViewController {
    func getSpeakerVals() -> [SpeakerVals] {
        if path == nil {return [SpeakerVals]()}
        
        var vals:(Double, Double) = (0,0)
        var points = [Point]()
        interpVal = 0
        interpTick = (path!.pointsM1() / path!.time) * app.resolution
        
        // get point vals for lines
        for (var i=0; interpVal<=path!.pointsM1(); i++) {
            interpForLine(&vals)
            points.append(Point(at: vals))
        }
        
        return getValsForSpeakers(points)
    }
    
    
    private func interpForLine(inout vals: (Double, Double)) {
        if (interpVal != 0 && interpVal >= path!.pointsM1()) {
            let point = path!.points.last!
            vals = (Double(point.x), Double(point.y))
            
            if path!.loop {interpVal = 0}
            // else stop path //TODO: fix
            return
        }
        
        let pt0 = path!.points[Int(interpVal)]
        let pt1 = path!.points[Int(interpVal+1)]
        let interpInt = Double(Int(interpVal))
        
        // set vals
        if pt0.x != pt1.x {
            vals.0 = (Double(pt1.x-pt0.x) * Double(interpVal-interpInt)) + Double(pt0.x)
            vals.1 = interpolateLinear(vals.0, pt0: pt0.center, pt1: pt1.center)
        } else {
            vals.0 = Double(pt1.x)
            vals.1 = (Double(pt1.y-pt0.y) * Double(interpVal-interpInt)) + Double(pt0.y)
        }
        
        interpVal += interpTick
        ldprint("\(vals.0.format(".2")), \(vals.1.format(".2"))", line: __LINE__, toggle: dbMapVC)
    }
    
    
    private func getValsForSpeakers(points: [Point]) -> [SpeakerVals] {
        var speakers = [SpeakerVals]()
        for speaker in map!.speakers {
            var spkVals = SpeakerVals()
            for point in points {
                if clickCheck(point.drawPoint, p2: speaker.radius.origin, d: speaker.size) {
                    var a = Double(point.x - speaker.center.x)
                    let b = Double(point.y - speaker.center.y)
                    a = -(sqrt(a*a+b*b) / Double(speaker.size*0.5)) + 1
                    if a>1 {a=1}
                    spkVals.vals.append(ampToDB(a))
                } else {spkVals.vals.append(-5000)}
            }
            spkVals.output = speaker.output
            speakers.append(spkVals)
        }
        return speakers
    }
}






// MARK: - Building
extension MapViewController {
    func build() {
        
    }
}






















