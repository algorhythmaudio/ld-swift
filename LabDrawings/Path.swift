import Cocoa

let dbPath: Bool = no


class Path: NSObject, NSCoding {
    var name: String = "New Path"
    var loop: Bool = no
    var time: Double = 5
    var selected = -1
    var points = [Point]()
    
    override init() {
        super.init()
    }
    
    
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(name, forKey: "name")
        aCoder.encodeBool(loop, forKey: "loop")
        aCoder.encodeDouble(time, forKey: "time")
        aCoder.encodeObject(NSKeyedArchiver.archivedDataWithRootObject(points), forKey: "points")
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init()
        
        name = aDecoder.decodeObjectForKey("name") as! String
        loop = aDecoder.decodeBoolForKey("loop")
        time = aDecoder.decodeDoubleForKey("time")
        
        // set points
        if let data = aDecoder.decodeObjectForKey("points") as? NSData {
            if let newPoints = NSKeyedUnarchiver.unarchiveObjectWithData(data) as? [Point] {
                points = newPoints
                ldprint("|\tspeakers: \(points)", line: __LINE__, toggle: dbPath)
                for point in points {
                    ldprint("|\t|\tcenter: \(point.center)", line: __LINE__, toggle: dbPath)
                }
            } else {print("Error: couldn't unarchive points in a Path")}
        } else {print("Error: couldn't decode 'points' in path")}
    }
}



// xib
extension Path {
    func draw() {
        if points.count == 0 {return}
        drawLine()
        drawPoints()
    }
    
    
    func drawLine() {
        if points.count > 1 {
            let point = points[0]
            
            // draw line first
            let line = NSBezierPath()
            line.moveToPoint(point.center)
            for point in points {line.lineToPoint(point.center)}
            line.lineWidth = 2.0
            LDColor.pathLine()
            line.stroke()
        }
    }
    
    
    func drawPoints() {for point in points {point.draw()}}
}


extension Path {
    func addPointAt(point: NSPoint) {
        points.append(Point(center: point))
    }
    
    
    func getSelectedPoint() -> Point? {
        if selected == -1 || points.isEmpty {return nil}
        return points[selected]
    }
    
    
    func pointsM1() -> Double {
        if points.isEmpty {return 0}
        return Double(points.count-1)
    }
}
























