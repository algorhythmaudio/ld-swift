import Cocoa

let dbPathAnalysis = no


// struct for data
struct SpeakerInflectionInfo {
    var inflInfo = [Inflection]()
    var output: Int = 0
    var inflCount: Int {get{return inflInfo.count}}
}


struct Inflection {
    var start: Int = 0
    var end: Int = 0
    var val: Double = 0
}


struct Slope {
    var val: Double = 0
    var slope: Double = 0
    var tick: Int = 0
    var tickm1: Int = 0
    
    mutating func findSlope(speakerVals: SpeakerVals) {
        tickm1 = tick
        tick++
        slope = speakerVals.vals[tick] - val
        val = speakerVals.vals[tick]
    }
}



class PathAnalysis: NSObject {
    private var slope = Slope()
    var data = [SpeakerInflectionInfo]()
    
    func run(rawSpkVals: [SpeakerVals]) -> Bool {
        for speakerVals in rawSpkVals {
            ldprint("********** Speaker \(speakerVals.output) **********", line: __LINE__, toggle: dbPathAnalysis)
            
            // find first infl
            slope.tick = -1
            slope.val = speakerVals.vals[0]
            repeat {slope.findSlope(speakerVals)}
                while (slope.slope == 0 && slope.tick < speakerVals.vals.count-1)
            
            // populate inflection info
            var speakerInfo = SpeakerInflectionInfo()
            if slope.slope == 0 {speakerInfo.inflInfo = dataWithConstLevel()}
            else                {speakerInfo.inflInfo = dataWithFades(speakerVals)}
            
            speakerInfo.output = speakerVals.output
            data.append(speakerInfo)
        }
        
        if data.isEmpty {return yes}
        else {return no}
    }
}





// MARK: - Utility
private extension PathAnalysis {
    func dataWithConstLevel() -> [Inflection] {
        ldprint("|\tSlope was constant at: \(slope.val.format(".2"))", line: __LINE__, toggle: dbPathAnalysis)
        
        var infl = Inflection()
        infl.val = slope.val
        return [infl]
    }
    
    
    func dataWithFades(speakerVals: SpeakerVals) -> [Inflection] {
        ldprint("|\tStarting slope \(slope.slope.format(".4")) for points: \(slope.tickm1), \(slope.tick)", line: __LINE__, toggle: dbPathAnalysis)
        
        var inflections = [Inflection]()
        
        // first find inflections
        let inflPts = findInflections(speakerVals)
        
        // build first fade
        var infl = Inflection()
        infl.end = inflPts[0]
        infl.val = speakerVals.vals[0]
        inflections.append(infl)
        
        ldprint("|\tStarting at \(infl.val.format(".2")) until \(infl.end)", line: __LINE__, toggle: dbPathAnalysis)
        
        // build other fade info
        var x, y, z: Int
        x = inflPts[0]
        z = 0
        for (var i=1; i<inflPts.count; i++) {
            y = inflPts[i]
            // test for setting prewait on next infl and skipping this infl
            if y<0 {z = (-y) - x; y = -y;}
            else {
                var newInfl = Inflection()
                newInfl.start = z
                newInfl.end = y-x
                newInfl.val = speakerVals.vals[y]
                
                inflections.append(newInfl)
                z = 0
                ldprint("|\t|\tInflection from \(x)-\(y)\t length \(y-x) to value \(newInfl.val.format(".4"))", line: __LINE__, toggle: dbPathAnalysis)
            }
            x=y
        }
        
        return inflections
    }
    
    
    func findInflections(speakerVals: SpeakerVals) -> [Int] {
        var array = [slope.tickm1]
        let count = speakerVals.vals.count - 1
        
        repeat {
            if slope.slope > 0 {
                repeat {slope.findSlope(speakerVals)} while (slope.slope>0 && slope.tick<count); array.append(slope.tickm1)
            }
            else if slope.slope < 0 {
                repeat {slope.findSlope(speakerVals)} while (slope.slope<0 && slope.tick<count); array.append(slope.tickm1)
            }
            else {
                repeat {slope.findSlope(speakerVals)} while (slope.slope==0 && slope.tick<count); array.append(-slope.tickm1)
            }
            ldprint("|\t|\tSlope \(slope.slope.format(".4")) \tindex: \(slope.tick)", line: __LINE__, toggle: dbPathAnalysis)
        } while slope.tick < count
        
        // ensure that last element is the last level value
        let last = array.last
        array.removeLast()
        array.append(last!+1)
        
        return array
    }
}



















