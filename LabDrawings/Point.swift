import Cocoa


class Point: NSObject, NSCoding {
    var rect: NSRect {get{return _rect}}
    var drawPoint: NSPoint {get{return _rect.origin}}
    
    var size: CGFloat = 7.5 {didSet {update()}}
    var center: NSPoint = NSMakePoint(0, 0) {didSet {update()}}
    var color: NSColor = LDColor.pathPoint()
    var x: CGFloat {get{return center.x}}
    var y: CGFloat {get{return center.y}}
    
    private var _rect: NSRect = NSMakeRect(0, 0, 0, 0)
    private var _drawPoint: NSPoint = NSMakePoint(0, 0)
    
    
    init(center: NSPoint) {
        super.init()
        self._rect.size = NSMakeSize(size, size)
        self.center = center
        update()
    }
    
    
    init(at: (Double, Double)) {
        super.init()
        self._rect.size = NSMakeSize(size, size)
        self.center = NSMakePoint(CGFloat(at.0), CGFloat(at.1))
        update()
    }
    
    
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodePoint(center, forKey: "center")
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init()
        center = aDecoder.decodePointForKey("center")
        update()
    }
}


// Drawing
extension Point {
    func draw() {
        let draw = NSBezierPath(ovalInRect: _rect)
        color.set()
        draw.fill()
        LDColor.pointBorder()
        draw.lineWidth = 1.0
        draw.stroke()
    }
}


// Utility
private extension Point {
    func update() {
        let sizeHalf = size*0.5
        _rect = NSMakeRect(center.x-sizeHalf, center.y-sizeHalf, size, size)
    }
}




