import Cocoa

let dbSpeaker: Bool = no
let maxRadiusSize:CGFloat = 300.0
let minRadiusSize:CGFloat = 100.0
let speakerSize:CGFloat = 13.0


class Speaker: NSObject, NSCoding {
    // data
    var level:Double = -2000 // obscure number to force label update
    var output:Int = 0
    var isSelected:Bool = no
    var size:CGFloat = maxRadiusSize {
        didSet {
            if size < minRadiusSize {size = minRadiusSize}
            else if size > maxRadiusSize {size = maxRadiusSize}
            updateSizeRadius(size)
            updateSizePower(size)
            updateFromCenter()
        }
    }
    
    // drawing
    var center:NSPoint = NSMakePoint(0, 0) {didSet {updateFromCenter()}}
    private var _radius: NSRect = NSMakeRect(0, 0, 0, 0)
    private var _power: NSRect = NSMakeRect(0, 0, 0, 0)
    private var _speaker: NSRect = NSMakeRect(0.0, 0.0, speakerSize, speakerSize)
    
    // getters
    var radius: NSRect {get {return _radius}}
    var power: NSRect {get {return _power}}
    var speaker: NSRect {get {return _speaker}}
    
    
    
    // creators
    init(point: NSPoint, forOutput out: Int) {
        super.init()
        center = point
        output = out
        updateFromCenter()
        updateSizes()
    }
    
    
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeInteger(output, forKey: "output")
        aCoder.encodeFloat(Float(maxRadiusSize), forKey: "size")
        aCoder.encodePoint(center, forKey: "center")
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init()
        
        output = aDecoder.decodeIntegerForKey("output")
        size = CGFloat(aDecoder.decodeFloatForKey("size"))
        center = aDecoder.decodePointForKey("center")
        
        updateSizes()
        updateFromCenter()
    }
}






// MARK: - Local Implementation

private extension Speaker {
    func updateFromCenter() {
        updateSpeaker(center.x, y: center.y)
        updateRadius(center.x, y: center.y)
        updatePower(center.x, y: center.y)
    }
    
    func updateSizes() {
        updateSizePower(size)
        updateSizeRadius(size)
    }
    
    func updateSpeaker(x: CGFloat, y: CGFloat) {
        _speaker.origin.x = x - (_speaker.size.height*0.5)
        _speaker.origin.y = y - (_speaker.size.width*0.5)
    }
    
    func updateRadius(x: CGFloat, y: CGFloat) {
        let sizeHalf = size*0.5
        _radius.origin.x = x - sizeHalf
        _radius.origin.y = y - sizeHalf
    }
    
    func updateSizeRadius(val: CGFloat) {
        _radius.size.height = val
        _radius.size.width = val
    }
    
    func updatePower(x: CGFloat, y: CGFloat) {
        let powerOrigin = (size*0.5)*0.293
        _power.origin.x = center.x - powerOrigin
        _power.origin.y = center.y - powerOrigin
    }
    
    func updateSizePower(val: CGFloat) {
        let x = val*0.293
        _power.size.height = x
        _power.size.width = x
    }
}





// MARK: - Drawing

extension Speaker {
    // drawing
    func drawAll() {
        ldprint("Speaker [\(output)]\n\tSpeaker \(speaker)\n\tPower \(power)\n\tRadius \(radius)", line: __LINE__, toggle: dbSpeaker)
        drawRadius()
        drawPower()
        drawSpeaker()
    }
    
    
    func drawSpeaker() {
        ldprint("Speaker [\(output)]\n\tSpeaker \(speaker)", line: __LINE__, toggle: dbSpeaker)
        let drawSpeaker = NSBezierPath(ovalInRect: speaker)
        LDColor.speaker(isSelected)
        drawSpeaker.fill()
        LDColor.outlineSpeaker(isSelected)
        drawSpeaker.lineWidth = 2
        drawSpeaker.stroke()
    }
    
    func drawPower() {
        ldprint("Speaker [\(output)]\n\tPower \(power)", line: __LINE__, toggle: dbSpeaker)
        let drawPower = NSBezierPath(ovalInRect: power)
        LDColor.power(isSelected)
        drawPower.fill()
        LDColor.outlinePower(isSelected)
        drawPower.lineWidth = 1
        drawPower.stroke()
    }
    
    func drawRadius() {
        ldprint("Speaker [\(output)]\n\tRadius \(radius)", line: __LINE__, toggle: dbSpeaker)
        let drawRadius = NSBezierPath(ovalInRect: radius)
        LDColor.radius(isSelected)
        drawRadius.fill()
        LDColor.outlineRadius(isSelected)
        drawRadius.lineWidth = 2
        drawRadius.stroke()
    }
}




















