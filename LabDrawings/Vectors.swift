import Foundation

/* Note: This code was found on: http://msdn.microsoft.com/en-us/library/ms969920.aspx
It discuses how to find a mouse click near a line by using vectors, this is called
hit testing.  To clairify the choice of the variable names and the vernacular used:

Vector A = imaginary line from the first actual point and the mouse click point.
Vector B = actual line from the first to second point.
Vector C = this is vector A projected onto vecton B.
Vector D = this is what we are looking for.  It is a perpendictular line from the
end of vector C to the mouse click point.  If this Vector is under a
certain length, then the user has clicked near the line.
*/


typealias Vector = (CGFloat, CGFloat)

let DIST: CGFloat = 7


func isClickNearLine(mouse: NSPoint, point1 pt1: NSPoint, point2 pt2: NSPoint) -> Bool {
    var A, B: Vector
    var p1 = pt1
    var p2 = pt2
    
    // ensure pt1 is left most point
    if p1.x > p2.x {
        let temp = p2
        p2 = p1
        p1 = temp
    }
    
    // build vectors
    A.0 = mouse.x - p1.x
    A.1 = mouse.y - p1.y
    B.0 = p2.x - p1.x
    B.1 = p2.y - p1.y
    
    // first check
    if (A.0+A.1 > B.0+B.1) {return no}
    
    // get vector e
    let e = mouseDistanceLength(A, B: B)
    
    // check condition of (+/-)e to check
    if (e >= -DIST && e <= DIST) {
        return mouseCheck(mouse, p1: p1, p2: p2)
    }
    
    return no
}





private func mouseDistanceLength(A: Vector, B: Vector) -> CGFloat {
    var C: Vector
    
    let prod = (dotProduct(A, v1: B) / dotProduct(B, v1: B))
    C.0 = B.0*prod
    C.1 = B.1*prod
    
    return distanceVector(subtractVectors(A, v1: C))
}


private func dotProduct(v0: Vector, v1: Vector) -> CGFloat {
    return ((v0.0 == 0 && v0.1 == 0) || (v1.0 == 0 && v1.1 == 0)) ? 0.0 : (v0.0 * v1.0) + (v0.1 * v1.1);
}


private func subtractVectors(v0: Vector, v1: Vector) -> Vector {
    var vec: Vector
    
    if (v0.0 == 0 && v0.1 == 0) || (v1.0 == 0 && v1.1 == 0) {
        vec.0 = 0
        vec.1 = 0
        return vec
    }
    
    vec.0 = v0.0 - v1.0
    vec.1 = v0.1 - v1.1
    return vec
}


private func distanceVector(v: Vector) -> CGFloat {
    if v.0 == 0 && v.1 == 0 {return 0}
    return sqrt((v.0*v.0) + (v.1*v.1))
}


private func mouseCheck(mouse: NSPoint, p1: NSPoint, p2: NSPoint) -> Bool {
    if ((mouse.x > p1.x && mouse.x > p2.x) || (mouse.x < p1.x && mouse.x < p2.x)) {
        if ((mouse.y > p1.y && mouse.y > p2.y) || (mouse.y < p1.y && mouse.y < p2.y)) {
            return no;
        }
    }
    return yes
}

























