//
//  PathAnalysisTest.swift
//  LabDrawings
//
//  Created by GW Rodriguez on 3/14/16.
//  Copyright © 2016 GW Rodriguez. All rights reserved.
//

import XCTest
@testable import LabDrawings


class PathAnalysisTest: XCTestCase {
    var analysis = PathAnalysis()
    

    override func setUp() {
        super.setUp()
        
    }
    
    override func tearDown() {
        
        super.tearDown()
    }

//    func testPerformanceExample() {
//        // This is an example of a performance test case.
//        self.measureBlock {
//            // Put the code you want to measure the time of here.
//        }
//    }
    
    
    func testPathAnalysis() {
        // check analysis
        analysis.run(createSpkVals())
        let testData = createInflections()
        XCTAssertEqual(analysis.data.count, testData.count, "Number of speaker data failed")
        
        // do test
        for (data, tdata) in zip(analysis.data, testData) {
            XCTAssertEqual(data.inflCount, tdata.inflCount, "Inflection counts off: \(data.inflCount):\(tdata.inflCount)")
            XCTAssertEqual(data.output, tdata.output, "Outputs are off: \(data.output):\(tdata.output)")
            
            for (info, tinfo) in zip(data.inflInfo, tdata.inflInfo) {
                XCTAssertEqual(info.start, tinfo.start, "Speaker \(data.output) inflection start missmatch: \(info.start):\(tinfo.start)")
                XCTAssertEqual(info.end, tinfo.end, "Speaker \(data.output) inflection start missmatch: \(info.end):\(tinfo.end)")
                XCTAssertEqual(info.val, tinfo.val, "Speaker \(data.output) inflection start missmatch: \(info.val):\(tinfo.val)")
            }
        }
    }
}







// testing data
extension PathAnalysisTest {
    func createSpkVals() -> [SpeakerVals] {
        var val1 = SpeakerVals()
        val1.output = 1
        for _ in 0...20 {val1.vals.append(-5000)}
        val1.vals.appendContentsOf([-59.74696874481466, -42.79833660992742, -33.86181258756218, -27.7539131983046, -23.10881389623617, -19.36017638734432, -16.21807912024061, -13.5142119842135, -11.14235640713978, -9.031538337388721, -7.132692579360862, -5.412067925066153, -3.849797964344404, -2.449570770619793, -1.315262904194199, -1.10921877960468, -2.091742884488724, -3.437225837827106, -4.955854010126372])
        
        var val2 = SpeakerVals()
        val2.output = 2
        for _ in 0...30 {val2.vals.append(-5000)}
        val2.vals.appendContentsOf([-68.28570671025494, -45.94410886661706, -35.79300074039815, -29.14727288288944, -24.19855030293567, -20.25478169460869, -16.97662797843905, -14.17237720896243, -11.72326112410586])
        
        return [val1, val2]
    }
    
    
    func createInflections() -> [SpeakerInflectionInfo] {
        // speaker 1
        var spkInfl1 = SpeakerInflectionInfo()
        spkInfl1.output = 1
        spkInfl1.inflInfo.append(Inflection(start: 0, end: 20, val: -5000))
        spkInfl1.inflInfo.append(Inflection(start: 0, end: 16, val: -1.10921877960468))
        spkInfl1.inflInfo.append(Inflection(start: 0, end: 3, val: -4.955854010126372))
        
        // speaker 2
        var spkInfl2 = SpeakerInflectionInfo()
        spkInfl2.output = 2
        spkInfl2.inflInfo.append(Inflection(start: 0, end: 30, val: -5000))
        spkInfl2.inflInfo.append(Inflection(start: 0, end: 9, val: -11.72326112410586))
        
        return [spkInfl1, spkInfl2]
    }
}

















